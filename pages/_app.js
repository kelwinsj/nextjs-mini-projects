import React from 'react';
import Head from 'next/head';
import { ThemeProvider, CSSReset } from '@chakra-ui/core';

const MyApp = ({ Component, pageProps }) => {
  return (
    <ThemeProvider>
      <Head>
        <title>Next JS Chakra UI</title>
        <meta name='viewport' content='initial-scale=1.0, width=device-width' />
      </Head>
      <Component {...pageProps} />
      <CSSReset />
    </ThemeProvider>
  );
};

export default MyApp;
