import React from 'react';
import { motion } from 'framer-motion';
import {
  Box,
  Stack,
  FormControl,
  InputGroup,
  Input,
  Image,
  Flex,
  Button,
  Text,
  Link,
  Heading,
} from '@chakra-ui/core';

function Login() {
  return (
    <motion.div
      initial='hidden'
      animate='visible'
      variants={{
        hidden: {
          scale: 0.9,
          opacity: 0,
        },
        visible: {
          scale: 1,
          opacity: 1,
          transition: {
            delay: 0.4,
            duration: 0.4,
            ease: 'easeInOut',
          },
        },
      }}
    >
      <Box w='500px' bg='gray.200' px={8} py={16} boxShadow='sm' rounded='sm'>
        <Flex mb={4} justify='center'>
          <Image src='./logo.png' w='100px' rounded='full' shadow='sm' />
        </Flex>
        <Box my={3}>
          <Heading as='h1' size='md'>
            This is simple NextJS Project
          </Heading>
          <Text>
            It also works with Chakra UI and Framer motion for simple animations
          </Text>
        </Box>
        <form>
          <Stack spacing={3}>
            <FormControl isRequired>
              <InputGroup>
                <Input
                  type='name'
                  placeholder='First Name'
                  aria-label='First Name'
                />
              </InputGroup>
            </FormControl>
            <FormControl isRequired>
              <InputGroup>
                <Input
                  type='name'
                  placeholder='Last Name'
                  aria-label='Last Name'
                />
              </InputGroup>
            </FormControl>
            <FormControl isRequired>
              <InputGroup>
                <Input type='email' placeholder='Email' aria-label='Email' />
              </InputGroup>
            </FormControl>
            <FormControl isRequired>
              <InputGroup>
                <Input
                  type='password'
                  placeholder='Password'
                  aria-label='Password'
                />
              </InputGroup>
            </FormControl>
            <Button
              type='submit'
              bg='blue.900'
              color='gray.100'
              transition='ease-in-out'
              _hover={{ boxShadow: 'lg', bg: 'teal.600' }}
            >
              Sign Up
            </Button>

            <Text textAlign='center' mt={3}>
              Or try going to our{' '}
              <Link
                fontWeight='bold'
                textDecoration='none'
                _hover={{ color: 'red.600' }}
                href='/recipes'
              >
                recipes*
              </Link>{' '}
              page
            </Text>
          </Stack>
        </form>
      </Box>
    </motion.div>
  );
}

export default Login;
