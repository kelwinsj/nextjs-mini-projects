import { Box, Heading, Flex } from '@chakra-ui/core';
// import Navbar from './components/navbar';
import Login from './components/login';

export default function Home() {
  return (
    <Flex w="100%" h="100vh" justify="center" align="center">
      <Login />
    </Flex>
  );
}
