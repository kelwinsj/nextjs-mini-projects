import { useForm } from 'react-hook-form';
import React, { useState } from 'react';
import NextLink from 'next/link';
import {
  FormErrorMessage,
  FormLabel,
  FormControl,
  Input,
  Button,
  Flex,
  Box,
  Link,
  Text,
} from '@chakra-ui/core';

export default function HookForm() {
  const { handleSubmit, errors, register, formState } = useForm();

  function validateName(value) {
    let error;
    if (!value) {
      error = 'Name is required';
    } else if (value !== 'Steve') {
      error = "Jeez! You're not a fan 😱";
    }
    return error || true;
  }

  function onSubmit(values) {
    setTimeout(() => {
      alert(JSON.stringify(values, null, 2));
    }, 1000);
  }

  return (
    <Flex bg='blue.100' h='100vh' w='full' justify='center' align='center'>
      <Box w='500px' bg='blue.100' py={14} px={10} rounded='lg' shadow='md'>
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormControl isInvalid={errors.name}>
            <FormLabel htmlFor='name' color='blue.900'>
              Enter your first name
            </FormLabel>
            <Input
              bg='blue.100'
              border='1px'
              borderColor='blue.900'
              name='name'
              placeholder='Name'
              ref={register({ validate: validateName })}
            />
            <FormErrorMessage>
              {errors.name && errors.name.message}
            </FormErrorMessage>
          </FormControl>
          <Button
            mt={4}
            bg='blue.900'
            variantColor='teal'
            isLoading={formState.isSubmitting}
            type='submit'
            _hover={{ bg: 'gray.800' }}
          >
            Submit
          </Button>
        </form>
        <Flex mt={5} mb={3}>
          Hint...try Steve
        </Flex>

        <Text>
          Or try going{' '}
          <Link
            fontWeight='bold'
            textDecoration='none'
            _hover={{ color: 'red.600' }}
            href='/'
          >
            home
          </Link>
        </Text>
      </Box>
    </Flex>
  );
}
